var ethers = require('ethers');

const MyBankDangerousWithGuard = artifacts.require("MyBankDangerousWithGuard");

module.exports = async function (deployer, network, accounts) {
    // get instance of deployed contract
    await deployer.deploy(MyBankDangerousWithGuard);
    const bankDangerousWithGuard = await MyBankDangerousWithGuard.deployed();

    await bankDangerousWithGuard.deposit({from: accounts[2], value: ethers.utils.parseUnits("10", "ether").toHexString()})
    await bankDangerousWithGuard.deposit({from: accounts[3], value: ethers.utils.parseUnits("10", "ether").toHexString()})
    await bankDangerousWithGuard.deposit({from: accounts[4], value: ethers.utils.parseUnits("10", "ether").toHexString()})

    console.log("Deployed MyToken Token");
};