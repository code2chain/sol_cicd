const MyBankDangerous = artifacts.require("MyBankDangerous");
const MyBankSafe = artifacts.require("MyBankSafe");
var ethers = require('ethers');

module.exports = async function (deployer, network, accounts) {
    await deployer.deploy(MyBankDangerous);
    await deployer.deploy(MyBankSafe);

    // get instance of deployed contract
    const bankDangerous = await MyBankDangerous.deployed();
    const bankSafe = await MyBankSafe.deployed();

    await bankDangerous.deposit({from: accounts[2], value: ethers.utils.parseUnits("20", "ether").toHexString()})
    await bankDangerous.deposit({from: accounts[3], value: ethers.utils.parseUnits("20", "ether").toHexString()})
    await bankDangerous.deposit({from: accounts[4], value: ethers.utils.parseUnits("20", "ether").toHexString()})

    await bankSafe.deposit({from: accounts[2], value: ethers.utils.parseUnits("20", "ether").toHexString()})
    await bankSafe.deposit({from: accounts[3], value: ethers.utils.parseUnits("20", "ether").toHexString()})
    await bankSafe.deposit({from: accounts[4], value: ethers.utils.parseUnits("20", "ether").toHexString()})

    console.log("Deployed MyToken Token");
};