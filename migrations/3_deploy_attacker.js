var ethers = require('ethers');

const Attacker = artifacts.require("Attacker");
const MyBankDangerous = artifacts.require("MyBankDangerous");
const MyBankSafe = artifacts.require("MyBankSafe");

module.exports = async function (deployer, network, accounts) {
    // get instance of deployed contract
    const bankDangerous = await MyBankDangerous.deployed();
    const bankSafe = await MyBankSafe.deployed();

    await deployer.deploy(Attacker, bankDangerous.address);
    const attacker = await Attacker.deployed();

    console.log("Deployed MyToken Token");
};