// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract MyBankSafe {
    mapping(address => uint256) private balances;

    function deposit() public payable returns (uint256) {
        balances[msg.sender] += msg.value;
        return balances[msg.sender];
    }

    function withdraw(uint256 withdrawAmount)
        public
        returns (uint256 remainingBal)
    {
        if (withdrawAmount <= balances[msg.sender]) {
            // deduct first
            balances[msg.sender] -= withdrawAmount;
            payable(msg.sender).transfer(withdrawAmount);
        }
        return balances[msg.sender];
    }

    function balance() public view returns (uint256) {
        return balances[msg.sender];
    }
}
