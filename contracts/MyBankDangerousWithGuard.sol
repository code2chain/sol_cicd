// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "@openzeppelin/contracts/security/ReentrancyGuard.sol";

contract MyBankDangerousWithGuard is ReentrancyGuard {
    event LogWithDraw(address withdrawer, uint256 amount);

    mapping(address => uint256) private balances;

    function deposit() public payable returns (uint256) {
        require(
            (balances[msg.sender] + msg.value) >= balances[msg.sender],
            "Deposit Must not reduce Balance"
        );
        balances[msg.sender] += msg.value;
        return balances[msg.sender];
    }

    function withdraw(uint256 withdrawAmount)
        external
        nonReentrant
        returns (uint256 remainingBal)
    {
        uint256 senderBalance = balances[msg.sender];
        require(senderBalance >= withdrawAmount, "Not enough amount");

        emit LogWithDraw(address(msg.sender), withdrawAmount);

        (bool sent, ) = payable(msg.sender).call{value: withdrawAmount}("");
        // payable(msg.sender).transfer(withdrawAmount);
        require(sent, "Failed to send Ether");

        balances[msg.sender] = senderBalance - withdrawAmount;
        return balances[msg.sender];
    }

    function balance() public view returns (uint256) {
        return balances[msg.sender];
    }
}
